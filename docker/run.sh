nvidia-docker run --rm -it \
			--ipc=host \
			--net=host \
			-v `pwd`/../:/src \
			-e DISPLAY=$DISPLAY \
			-v /tmp/.X11-unix:/tmp/.X11-unix \
			-v ~/.Xauthority:/root/.Xauthority \
			-v /home/dima/Pycharm:/pycharm \
			test-pytorch bash
