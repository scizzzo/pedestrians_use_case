import torch
from torch.utils.data import DataLoader
from torch.optim import Adam, SGD
from torch.nn import BCELoss
import argparse

from dataset import PersonsDataset
from metrics import Accuracy, BCEDiceLoss, Dice
from unet import get_unet
from resunet import get_resUnet
from train import train, evaluate
from os.path import join, exists
import os


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='mode')
    train_parser = subparsers.add_parser('train')
    eval_parser = subparsers.add_parser('eval')

    def add_args(p, is_train):
        p.add_argument('--data', type=str, help='Dirpath to data', required=True)
        p.add_argument('--classes', type=int, help='Number of classes excluding neutral class', required=True)
        p.add_argument('--size', type=int, help='Resize images to squares with given size before feeding to NN', required=True)
        p.add_argument('--output', type=str, help='Dirpath to store training output', required=True)
        p.add_argument('--weights', type=str, help='Filepath for "*.pt" weights file', required=(not is_train))
        if is_train:
            p.add_argument('--eval-data', dest='eval_data',  type=str)
            p.add_argument('--batch-size', dest='batch_size', type=int, required=True)
            p.add_argument('--epochs', type=int, help='Number of epochs to train', required=True)
            p.add_argument('--val-every', dest='val_freq', type=float, help='Validate every given epoch', required=True)
            p.add_argument('--lr', type=float, help='Learning rate', default=1e-2)
            p.add_argument('--patience', type=int, help='Decrease lr when min val is not improving for given validations', default=5)
            p.add_argument('-p', action='store_true', help='Use VGG pretrained weights', default=False)
        else:
            pass
    add_args(train_parser, True)
    add_args(eval_parser, False)
    return parser.parse_args()


def main():
    args = parse_args()

    assert args.size % 16 == 0
    input_shape = (args.size, args.size)

    data_root = args.data
    is_train = (args.mode == 'train')
    if is_train:
        train_dataloader = DataLoader(
            dataset= PersonsDataset(data_root, is_train),
            shuffle=True,
            batch_size=args.batch_size,
            num_workers=0
        )
    
    val_dataloader = DataLoader(
        dataset=PersonsDataset(args.eval_data, False),
        shuffle=False,
        batch_size=2,
        num_workers=0
    )

    if is_train:
        if args.weights is not None and args.p:
            raise Exception('Cannot use both VGG weights and specified weights')

    pretrained = args.p if is_train else False
    unet = get_unet(pretrained=pretrained).cuda()
    #unet = get_resUnet(pretrained=pretrained).cuda()
    if not exists(args.output):
        os.mkdir(args.output)
    if args.weights is not None:
        unet.load_state_dict(torch.load(args.weights))
        print('Restored model weights {}'.format(args.weights))
        
    criterion = BCEDiceLoss()
    metrics = {'dice': Dice()}
    if is_train:
        train(unet,
              args.output,
              Adam, args.lr,
              args.epochs,
              criterion,
              train_dataloader, val_dataloader,
              args.val_freq,
              metrics=metrics,
              patience=args.patience)
    else:
        evaluate(unet,
                 val_dataloader,
                 {'loss': criterion, **metrics})

if __name__ == '__main__':
    main()
