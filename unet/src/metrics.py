from torch import nn
import torch
import numpy as np
import torch.nn.functional as F
from scipy import ndimage


def dice_loss(preds, trues, weight=None, is_average=True):
    num = preds.size(0)
    preds = preds.view(num, -1)
    trues = trues.view(num, -1)
    if weight is not None:
        w = torch.autograd.Variable(weight).view(num, -1)
        preds = preds * w
        trues = trues * w
    intersection = (preds * trues).sum(1)
    scores = 2. * (intersection + 1) / (preds.sum(1) + trues.sum(1) + 1)

    if is_average:
        score = scores.sum()/num
        return torch.clamp(score, 0., 1.)
    else:
        return scores

def dice_clamp(preds, trues, is_average=True):
    preds = torch.round(preds)
    return dice_loss(preds, trues, is_average=is_average)

class DiceLoss(nn.Module):
    def __init__(self, size_average=True):
        super().__init__()
        self.size_average = size_average

    def forward(self, input, target, weight=None):
        return 1-dice_loss(F.sigmoid(input), target, weight=weight, is_average=self.size_average)

class BCEDiceLoss(nn.Module):
    def __init__(self, size_average=True):
        super().__init__()
        self.size_average = size_average
        self.dice = DiceLoss(size_average=size_average)

    def forward(self, input, target, weight=None):
        return nn.BCELoss(size_average=self.size_average, weight=weight)(input, target) + self.dice(input, target, weight=weight)

class Accuracy:
    def __init__(self, ignore_index=None):
        if ignore_index is not None:
            self.ignore_index = ignore_index

    def __call__(self, outputs, targets):
        outputs = outputs.data.cpu().numpy()
        targets = targets.data.cpu().numpy()
        outputs[outputs>=0.5] = 1
        outputs[outputs<0.5] = 0

        total_pixels = np.prod(outputs.shape)
        correct_pixels = np.sum((outputs == targets).astype(int))
        return correct_pixels / total_pixels

class Dice:
    def __init__(self, threshold = 0.5):
        self.threshold = threshold

    def __call__(self, outputs, targets):
        eps = 1e-5
        outputs = outputs.data.cpu().numpy()
        targets = targets.data.cpu().numpy()
        outputs[outputs < self.threshold] = 0
        outputs[outputs >= self.threshold] = 1
        nominator = np.sum(outputs*targets)
        return 2.*nominator/(np.sum(outputs) + np.sum(targets) + eps)

class Dice2:
    def __init__(self, threshold = 0.42, del_thresh=100):
        self.threshold = threshold
        self.del_thresh = del_thresh

    def __call__(self, outputs, targets):
        eps = 1e-5
        outputs = outputs.data.cpu().numpy()
        targets = targets.data.cpu().numpy()
        outputs[outputs < self.threshold] = 0
        outputs[outputs >= self.threshold] = 1
        mask_img = outputs[0][0]
        #print(mask_img.shape)
        labeled_array, num_features = ndimage.label(mask_img)
        unique, counts = np.unique(labeled_array, return_counts=True)
        for (k,v) in dict(zip(unique, counts)).items():
            if v < self.del_thresh:
                #print('sdf')
                mask_img[labeled_array == k] = 0
        mask_img[mask_img > 0] = 1
        nominator = np.sum(outputs*targets)
        return 2.*nominator/(np.sum(outputs) + np.sum(targets) + eps)
