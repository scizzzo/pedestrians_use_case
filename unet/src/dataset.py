import torch
from torch.utils.data import Dataset
import os
from os.path import join
import numpy as np
import cv2
from imgaug import augmenters as iaa
from PIL import Image
from torchvision.transforms import ToTensor, Normalize, Compose



normalizer = Compose([
    ToTensor(),
    Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])


class PersonsDataset(Dataset):
    def __init__(self, filepath, is_train):
        with open(filepath) as f:
            files = f.readlines()
        self.samples = [join('/src/', file.split('./')[1]) for file in files]
        self.data_dirpath = filepath
        self.is_train = is_train

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, indx):
        SIZE = 256
        sometimes = lambda aug: iaa.Sometimes(0.5, aug)
        seq = iaa.Sequential([
            iaa.Fliplr(.5),
            iaa.Flipud(.5),
            sometimes([iaa.OneOf([
                iaa.Affine(rotate=90),
                iaa.Affine(rotate=-90)
            ])])
        ])

        seq_det = seq.to_deterministic()

        sample_name = self.samples[indx].strip()
        #print(sample_name.strip())
        img = cv2.imread(sample_name)
        #print(img.shape)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        gt = cv2.imread(sample_name.split('img/')[0] + 'gt/' + sample_name.split('img/')[1])
        gt = gt[:,:,0]
        gt[gt>0] = 1
        img = cv2.resize(img, (SIZE, SIZE))
        gt = cv2.resize(gt, (SIZE, SIZE), interpolation = cv2.INTER_NEAREST)
        #if self.is_train:
            #img = seq_det.augment_image(img)
            #gt = seq_det.augment_image(gt)
        #print(img.shape, gt.shape)
        #print(np.sum(gt))
        #gt = cv2.resize(gt, (512, 512))
        return normalizer(img), torch.from_numpy(gt.astype('f4')).unsqueeze_(0)


