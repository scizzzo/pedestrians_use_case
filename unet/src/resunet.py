import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models import resnet50
import os

def get_resUnet(pretrained = False):
    resnet_filepath = './models/resnet50.pt'
    if os.path.exists(resnet_filepath):
        model = resnet50(pretrained=False)
        if pretrained:
            model.load_state_dict(torch.load(resnet_filepath))
    else:
        model = resnet50(pretrained=pretrained)
        if pretrained:
            if not os.path.exists(os.path.dirname(resnet_filepath)):
                os.mkdir(os.path.dirname(resnet_filepath))
            torch.save(model.state_dict(), resnet_filepath)

    blocks_list = ['relu', 'layer1', 'layer2', 'layer3', 'layer4']
    encoder_channels = [64, 256, 512, 1024, 2048]
    encoder_blocks = []
    cur_block = nn.Sequential()
    for name, child in model.named_children():
        if name == 'conv1':
            new_conv = nn.Conv2d(3, 64, kernel_size=(7,7), stride=(1,1), padding=(3,3))
            new_conv.weight = nn.Parameter(child.weight.data.cuda())
            cur_block.add_module('conv1', new_conv)
        else:
            cur_block.add_module(name, child)
        if name in blocks_list:
            encoder_blocks.append(cur_block.cuda())
            cur_block = nn.Sequential()
    return Unet(encoder_blocks, encoder_channels)


class UpBlock(nn.Module):
    def __init__(self, bottom_channels, out_channels):
        super().__init__()
        self.up = nn.Upsample(scale_factor= 2, mode = 'nearest')
        self.conv = nn.Conv2d(bottom_channels, out_channels, kernel_size=1)

    def forward(self, bottom):
        up_features = self.up(bottom)
        return self.conv(up_features)


class DecodeBlock(nn.Module):
        def __init__(self, in_channels, out_channels):
            super().__init__()
            self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1)
            self.bn1 = nn.BatchNorm2d(out_channels)
            self.act1 = nn.ReLU()
            self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1)
            self.bn2 = nn.BatchNorm2d(out_channels)
            self.act2 = nn.ReLU()
            self.conv3 = nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1)
            self.bn3 = nn.BatchNorm2d(out_channels)
            self.act3 = nn.ReLU()

        def forward(self, input, left):
            x = torch.cat((input, left), dim=1)
            x = self.conv1(x)
            x = self.act1(self.bn1(x))
            x = self.conv2(x)
            x = self.act2(self.bn2(x))
            x = self.conv3(x)
            x = self.act3(self.bn3(x))
            return x

class Bottleneck(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
    def forward(self, x):
        return x


class Unet(nn.Module):
    def __init__(self, encoder, encoder_channels):
        super().__init__()
        self.encoder = encoder
        self.decoder_blocks = []
        self.up_blocks = []
        for i in range(len(encoder_channels)-1):
            self.decoder_blocks.append(DecodeBlock(encoder_channels[-(i+2)]*2, encoder_channels[-(i+2)]).cuda())
            self.up_blocks.append(UpBlock(encoder_channels[-(i+1)], encoder_channels[-(i+2)]).cuda())
        self.last_conv = nn.Conv2d(encoder_channels[0], 1, kernel_size=1)
        self.upsamle = nn.Upsample(scale_factor=2, mode='bilinear')
    def forward(self, x):
        encoder_output  = []
        for block in self.encoder:
            x = block(x)
            encoder_output.append(x)

        n = len(self.decoder_blocks)

        up = self.up_blocks[0](encoder_output[-1])
        for i in range(n-1):
            decode = self.decoder_blocks[i](up, encoder_output[-(i+2)])
            up = self.up_blocks[i + 1](decode)

        decode = self.decoder_blocks[-1](up, encoder_output[0])
        out = self.last_conv(decode)
        #out = self.upsamle(out)
        return F.sigmoid(out)
