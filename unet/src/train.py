from __future__ import division
from os.path import join
import shutil
from collections import defaultdict
import tqdm
from PIL import Image
import numpy as np
from torch.autograd import Variable


import torch

def train(model,
          output_dirpath,
          init_optimizer, lr,
          epochs,
          criterion,
          train_dataloader, val_dataloader,
          val_every,
          metrics={},
          patience=5):
    model.train()
    optimizer = init_optimizer(model.parameters(), lr=lr)
    best_val_loss = float('inf')
    next_val = val_every
    it = 0
    last_lr_reset_val_it = 0
    val_it = 0
    val_losses = []
    for epoch in range(epochs):
        n = len(train_dataloader)
        tq = tqdm.tqdm(total=n)
        tq.set_description('Epoch {}, lr {}'.format(epoch + 1, lr))
        tq.refresh()
        epoch_progress = 0
        for x, targets in train_dataloader:
            #print('sf')
            it += 1

            x = Variable(x).cuda()
            targets = Variable(targets).cuda()
            outputs = model(x)
            #print(outputs.size(), targets.size())
            loss = criterion(outputs, targets)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            postfix = {'loss': '{:.3f}'.format(loss.data[0])}
            for name, metric in metrics.items():
                metric_value = metric(outputs, targets)
                postfix[name] = metric_value
            tq.set_postfix(**postfix)
            tq.update(1)



            epoch_progress += 1
            global_progress = epoch + (epoch_progress / n)
            if global_progress + 1e-5 >= next_val:
                val_it += 1
                model_dirpath = join(output_dirpath, 'model.pt')
                torch.save(model.state_dict(), model_dirpath)
                while not next_val > global_progress:
                    next_val += val_every
                metric_values = evaluate(model,
                                         val_dataloader,
                                         metrics={'loss': criterion, **metrics})
                model.train()
                val_loss = metric_values['loss']
                val_losses.append(val_loss)
                if val_loss < best_val_loss:
                    best_val_loss = val_loss
                    best_model_dirpath = join(output_dirpath, 'model_best.pt')
                    shutil.copy(model_dirpath, best_model_dirpath)

        tq.close()


def evaluate(model, dataloader, metrics={}):
    model.eval()

    tq = tqdm.tqdm(total=len(dataloader))
    tq.set_description('Evaluating')
    tq.refresh()
    metric_values = defaultdict(int)
    samples = 0
    for x, targets in dataloader:
        x = Variable(x, volatile=True).cuda()
        targets = Variable(targets).cuda()
        outputs = model(x)
        batch_size = x.size(0)
        for name, metric in metrics.items():
            metric_value = metric(outputs, targets)
            if isinstance(metric_value, torch.autograd.Variable):
                metric_value = metric_value.data[0]
            metric_values[name] += metric_value * batch_size
        samples += batch_size
        tq.update(1)
    tq.close()
    for name in metric_values:
        print(name, metric_values[name]/samples )

    return metric_values
