python src/main.py train --output output_all --data /src/train.txt --eval-data /src/val.txt --classes 2 --size 256 --epochs 5 --lr 0.001 --val-every 1 --batch-size 8 --weights output_1/model_best.pt
